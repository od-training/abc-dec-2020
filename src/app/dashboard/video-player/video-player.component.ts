import { OnChanges, SimpleChanges } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video>;
  url: SafeResourceUrl | undefined;

  constructor(vds: VideoDataService, private sanitizer: DomSanitizer) {
    this.video = vds.selectedVideo.pipe(
      tap(
        (video) =>
          (this.url = this.sanitizer.bypassSecurityTrustResourceUrl(
            'https://www.youtube.com/embed/' + video.id
          ))
      )
    );
  }
  ngOnInit(): void {}
}
