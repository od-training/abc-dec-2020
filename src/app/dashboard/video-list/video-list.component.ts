import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css'],
})
export class VideoListComponent implements OnInit {
  videoList: Observable<Video[]>;
  selectedVideo: Observable<Video>;

  constructor(private vds: VideoDataService) {
    this.videoList = vds.videos;
    this.selectedVideo = vds.selectedVideo;
  }

  ngOnInit(): void {}

  pickVideo(video: Video): void {
    this.vds.setSelectedVideo(video);
  }
}
