import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from './dashboard/types';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  videos: Observable<Video[]>;
  selectedVideo = new Subject<Video>();

  constructor(private http: HttpClient) {
    this.videos = this.loadVideos();
  }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(
        map((videos) =>
          videos.map((v) => ({
            ...v,
            title: v.title.toUpperCase(),
          }))
        )
      );
  }

  setSelectedVideo(video: Video): void {
    this.selectedVideo.next(video);
  }
}
